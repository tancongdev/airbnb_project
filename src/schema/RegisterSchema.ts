import { z } from "zod"



export const RegisteSchema = z.object({
    name: z.string().nonempty('Vui lòng nhập tài khoản'),
    email: z.string().nonempty('Vui lòng nhập email').email(),
    password: z.string().nonempty('Vui lòng nhập mật khẩu'),
    phone: z.string().nonempty('Vui lòng nhập số điện thoại'),
    birthday: z.string().nonempty('Vui lòng nhập ngày sinh'),
    render: z.string().nonempty('Vui lòng nhập giới tính'),
    role: z.string().nonempty('Vui lòng nhập giới tính')
})


export type RegisteSchemaTypes = z.infer<typeof RegisteSchema>