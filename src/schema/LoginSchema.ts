import { z } from "zod";



export const loginSchema = z.object({
    email: z.string().nonempty('Vui lòng nhập email'),
    password: z.string().nonempty('Vui lòng nhập mật khẩu')
})

export type loginSchemaTypes = z.infer<typeof loginSchema>