import { apiInstance } from "constant/apiInstance";
import { loginSchemaTypes } from "schema/LoginSchema";
import { RegisteSchemaTypes } from "schema/RegisterSchema";
import { User } from "types/quanLyNguoiDung";



const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API
})

export const quanLyNguoiDungServices = {
    register: (payload: RegisteSchemaTypes) => api.post('/signup', payload),

    login: (payload: loginSchemaTypes) => api.post<ApiRespone<User>>('/signin', payload),

}