import { apiInstance } from "constant/apiInstance";
import { Room } from "types/quanLyPhong";


const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_PHONG_API
})


export const quanLyPhongServices = {
    getListRoom: () => api.get<ApiRespone<Room[]>>('/phong-thue'),
}