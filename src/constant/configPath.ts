export const PATH = {
    thanhPhoNoiTieng: '/thanh-pho-noi-tieng',
    hanok: '/hanok',
    khungCanhTuyetVoi: '/khung-canh-tuyet-voi',
    nhaNho: '/nha-nho',
    luotSong: '/luot-song',
    hoBoi: '/ho-boi',
    nhaTrenCay: '/nha-tren-cay',
    ryokan: '/ryokan',
    leuMucDong: '/leu-muc-dong',
    register: '/dangKy',
    login: '/dangNhap'


}