import { zodResolver } from "@hookform/resolvers/zod";
import { RegisteSchema, RegisteSchemaTypes } from "schema/RegisterSchema";
import { PATH } from "constant/configPath";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { quanLyNguoiDungServices } from "services/quanLyNguoiDung";
import { toast } from "react-toastify";

const RegisterTemplate = () => {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisteSchemaTypes>({
    mode: "onChange",
    resolver: zodResolver(RegisteSchema),
  });

  const onSubmit: SubmitHandler<RegisteSchemaTypes> = async (value) => {
      try {
        await quanLyNguoiDungServices.register(value);
        navigate(PATH.login)
        toast.success("Đăng ký thành công");
      } catch (err: any) {
        console.log('err: ', err);
        toast.error(err?.response?.data?.content)
      }
  };

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0 mt-28">
      <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Đăng Ký
          </h1>
          <form
            className="space-y-4 md:space-y-6 "
            onSubmit={handleSubmit(onSubmit)}
          >
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Họ tên"
                {...register('name')}
              />
              <p className="text-red-500">{errors?.name?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Email"
                {...register('email')}
              />
              <p className="text-red-500">{errors?.email?.message}</p>
            </div>
            <div>
              <input
                type="password"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Mật khẩu"
                {...register('password')}
              />
              <p className="text-red-500">{errors?.password?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Số điện thoại"
                {...register('phone')}
              />
              <p className="text-red-500">{errors?.phone?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Sinh nhật"
                {...register('birthday')}
              />
              <p className="text-red-500">{errors?.birthday?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Giới tính"
                {...register('render')}
              />
              <p className="text-red-500">{errors?.render?.message}</p>
            </div>
            <div>
              <input
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5"
                placeholder="Chức vụ"
                {...register('role')}
              />
              <p className="text-red-500">{errors?.role?.message}</p>
            </div>
            <div className="text-center">
              <button className=" text-white bg-blue-500 hover:bg-blue-700 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 w-full">
                Đăng ký
              </button>
            </div>

            <p className="text-sm font-light text-gray-500 dark:text-gray-400">
              Đã có tài khoản?{" "}
              <span
              onClick={()=> {
                navigate(PATH.login)
              }}
                className="text-blue-500 font-bold cursor-pointer"
              >
                Đăng nhập
              </span>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RegisterTemplate;
