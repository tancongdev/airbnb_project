import { HeartOutlined } from "@ant-design/icons"
import { useEffect } from "react"
import { useSelector } from "react-redux"
import { RootState, useAppDispatch } from "store"
import { getListRoomThunk } from "store/QuanLyPhim/thunk"
import Slider from "react-slick";


function NextArrow(props: { className: any; style: any; onClick: any }) {
  const { onClick } = props;
  return (
    <div
      className=" w-8 h-8 rounded-full flex justify-center items-center nextRight bg-white text-xs"
      onClick={onClick}
    >
      <i className="fa-solid fa-chevron-right"></i>
    </div>
  );
}

function PrevArrow(props: { className: any; style: any; onClick: any }) {
  const { onClick } = props;
  return (
    <div
      className=" w-8 h-8 rounded-full flex justify-center items-center bg-white text-xs nextLeft"
      onClick={onClick}
    >
      <i className="fa-solid fa-chevron-left"></i>
    </div>
  );
}

export const LeuMucDong = () => {

  const {listRoom} = useSelector((state: RootState) => state.quanLyPhongReducer)
  // console.log('listRoom: ', listRoom);

  const dispatch = useAppDispatch()

  useEffect(()=> {
    dispatch(getListRoomThunk())
  }, [dispatch])

  const listRoomByLocation = listRoom.filter((item)=> item.maViTri === 4)
  // console.log('listRoomByLocation: ', listRoomByLocation);

  const settings = {
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: (
      <NextArrow className={undefined} style={undefined} onClick={undefined} />
    ),
    prevArrow: (
      <PrevArrow className={undefined} style={undefined} onClick={undefined} />
    ),
  };
  return (
    <section className="text-gray-600 body-font max-w-[1400px] mx-auto">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap -m-4">
          {listRoomByLocation.map((room, i) => {
            return (
              <div className="lg:w-1/4 md:w-1/2 p-4 w-full " key={i}>
                <Slider {...settings} className="hoverImg cursor-pointer">
                  <div className="text-center rounded-2xl ">
                    <div className="flex justify-center ">
                      <div className="heartImg">
                        <HeartOutlined />
                      </div>
                      <img
                        className="homeImg"
                        src={room.hinhAnh}
                        alt={room.tenPhong}
                      />
                    </div>
                  </div>
                  <div className="text-center">
                    <div className="flex justify-center">
                      <div className="heartImg">
                        <HeartOutlined />
                      </div>
                      <img
                        className="homeImg"
                        src="https://a0.muscache.com/im/pictures/4588d88f-0224-42f4-94cb-594f4d362fba.jpg?im_w=720"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="text-center">
                    <div className="flex justify-center">
                      <div className="heartImg">
                        <HeartOutlined />
                      </div>
                      <img
                        className="homeImg"
                        src="https://a0.muscache.com/im/pictures/062ef52a-9b4f-4301-9413-e757d1758b3f.jpg?im_w=720"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="text-center">
                    <div className="flex justify-center">
                      <div className="heartImg">
                        <HeartOutlined />
                      </div>
                      <img
                        className="homeImg"
                        src="https://a0.muscache.com/im/pictures/e922f0c3-9a3d-4877-983a-56849ce92e18.jpg?im_w=720"
                        alt=""
                      />
                    </div>
                  </div>
                </Slider>
                <div className="mt-4 cursor-pointer">
                  <div className="flex justify-between">
                    <h2 className="text-gray-900 title-font text-[15px] font-medium">
                      {room.tenPhong}
                    </h2>
                    <div className="text-[12px]">
                      <i className="fa-solid fa-star">
                        <span className="text-[12px]">4.94</span>
                      </i>
                    </div>
                  </div>

                  <p className="py-2">{(room.moTa).substring(0, 60)} ...</p>
                  {/* <p>5 đêm . Ngày 28 tháng 10</p> */}
                  <p className="underline">
                    <span className="underline font-medium">${room.giaTien} </span>tổng
                    trước thuế
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>

  )
}

export default LeuMucDong
