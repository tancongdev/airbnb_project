import { Tabs } from "antd";
import type { TabsProps } from "antd";
import "./style.css";
import { BaiBien, DanhMuc, DayNui, NgoaiTroi, PhobienF, TraiNghiem, VanHoaVsNgheThuat } from "./Footer";




const onChange = (key: string) => {
  console.log(key);
};

const items: TabsProps["items"] = [
  {
    key: "1",
    label: "Phổ biến",
    children: <PhobienF />,
  },
  {
    key: "2",
    label: "Văn hóa và nghệ thuật",
    children: <VanHoaVsNgheThuat />,
  },
  {
    key: "3",
    label: "Ngoài trời",
    children: <NgoaiTroi />,
  },
  {
    key: "4",
    label: "Dãy núi",
    children: <DayNui />,
  },
  {
    key: "5",
    label: "Bãi biển",
    children: <BaiBien />,
  },
  {
    key: "6",
    label: "Danh mục",
    children: <DanhMuc />,
  },
  {
    key: "7",
    label: "Những điều nên trải nghiệm",
    children: <TraiNghiem />,
  },
];
export const FooterMain = () => {
  return (
    <footer className="text-gray-600 body-font bg-[#f7f7f7] ">
      <div className="max-w-[1400px] mx-auto py-6 px-8 ">
        <p className="text-black text-[22px] font-semibold pt-6">
          Nguồn cảm hứng cho những kỳ nghỉ sau này
        </p>
      </div>
      <div className="max-w-[1400px] mx-auto pb-10 px-8">
        <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
      </div>
      <hr />
      <div className="max-w-[1400px] mx-auto">
        <div className="container px-5 py-10 mx-auto">
          <div className="flex flex-wrap md:text-left text-center order-first">
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                Hỗ trợ
              </h2>
              <nav className="list-none mb-10 cursor-pointer">
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Trung tâm trợ giúp
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    AirCover
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Chống phân biệt đối xử
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Hỗ trợ người khuyết tật
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Các tùy chọn hủy
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Báo cáo lo ngại của khu dân cư
                  </a>
                </li>
              </nav>
            </div>
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                Đón tiếp khách
              </h2>
              <nav className="list-none mb-10 cursor-pointer">
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Cho thuê nhà trên Airbnb
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    AirCover cho Chủ nhà
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Tài nguyên về đón tiếp khách
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Diễn đàn cộng đồng
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Đón tiếp khách có trách nhiệm
                  </a>
                </li>
              </nav>
            </div>
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                Airbnb
              </h2>
              <nav className="list-none cursor-pointer">
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Trang tin tức
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Tính năng mới
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Cơ hội nghề nghiệp
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Nhà đầu tư
                  </a>
                </li>
                <li className="pb-2 ">
                  <a className="text-gray-600 hover:border-b-[1px] hover:border-black hover:text-black">
                    Chỗ ở khẩn cấp Airbnb.org
                  </a>
                </li>
              </nav>
            </div>
          </div>
        </div>
        <div className="border-t-2">
          <div className="container px-5 py-6 mx-auto flex items-center sm:flex-row flex-col">
            <p className="text-sm text-gray-500 sm:ml-6 sm:mt-0 mt-4">
              © 2023 Airbnb, Inc.
            </p>
            <a className="ml-2 text-sm" href="/">
              Quyền riêng tư -
            </a>
            <a className="ml-2 text-sm" href="/">
              Điều khoản -
            </a>
            <a className="ml-2 text-sm" href="/">
              Sơ đồ trang web
            </a>
            <div className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
              <div className="cursor-pointer">
                <i className="fa fa-globe"></i>
                <a className="text-black text-base ml-1 mr-4">Tiếng việt(VN)</a>
              </div>
              <div>
                <a href="/" className="text-sm mr-3 text-black">
                  $ USD
                </a>
              </div>
              <a className="text-[#222222]">
                <i className="fab fa-facebook-square text-lg"></i>
              </a>
              <a className="ml-3 text-[#222222]">
                <i className="fab fa-twitter-square text-lg"></i>
              </a>
              <a className="ml-3 text-[#222222]">
                <i className="fa-brands fa-square-instagram text-lg"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default FooterMain;
