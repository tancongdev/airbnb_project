import { Switch as SwitchA, SwitchProps as SwitchPropsA } from "antd";

type SwitchProps = SwitchPropsA & {

}


export const Switch = (props: SwitchProps) => {
    return <SwitchA {...props}/>
}


export default Switch