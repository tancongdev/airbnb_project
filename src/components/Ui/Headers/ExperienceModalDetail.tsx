

export const ExperienceModalDetail = () => {
  return (
    <div className="grid grid-cols-12 border-2 rounded-full m-3 w-[850px] ml-20 ">
      <div className="col-span-4 border-r-2 hover:bg-gray-300 hover:rounded-full py-3 pl-5 hover:transition hover:ease-in-out hover:delay-75">
        <h2 className="text-xs text-black font-semibold">Địa điểm</h2>
        <input type="text" placeholder="Tìm kiếm điểm đến" />
      </div>
      <div className="col-span-4 border-r-2 px-2 hover:bg-gray-300 hover:rounded-full py-3 pl-5 hover:transition hover:ease-in-out hover:delay-75">
        <h2 className="text-xs text-black font-semibold">Ngày</h2>
        <p className="text-sm text-gray-500">Thêm ngày</p>
      </div>
      <div className="col-span-4 pl-2 flex justify-between hover:bg-gray-300 hover:rounded-full py-3 hover:transition hover:ease-in-out hover:delay-75 pr-5">
        <div>
          <h2 className="text-xs text-black font-semibold">Khách</h2>
          <p className="text-sm text-gray-500">Thêm khách</p>
        </div>
        <div className="bg-[#ff385c] rounded-3xl p-3 text-white text-[15px] px-4">
          <i className="fa fa-search"></i>
        </div>
      </div>
    </div>
  )
}

export default ExperienceModalDetail
