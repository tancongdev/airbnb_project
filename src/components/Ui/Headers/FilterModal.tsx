import { Checkbox, Col, Row, Slider } from "antd";
import { CheckboxValueType } from "antd/es/checkbox/Group";
import "../style.css";
import { useEffect, useState } from "react";

const FilterModal = () => {
  const onChange = (checkedValues: CheckboxValueType[]) => {
    console.log("checked = ", checkedValues);
  };

  useEffect(() => {
    const handleClickBedRoom = document.querySelectorAll(".bedRoom");
    handleClickBedRoom.forEach((bedroom) => {
      bedroom.addEventListener("click", () => {
        document
          .querySelector(".activeBedroom")
          .classList.remove("activeBedroom");
        bedroom.classList.add("activeBedroom");
      });
    });

    const handleClickBed = document.querySelectorAll(".bed");
    handleClickBed.forEach((bed) => {
      bed.addEventListener("click", () => {
        document.querySelector(".activeBed").classList.remove("activeBed");
        bed.classList.add("activeBed");
      });
    });

    const handleClickBathRoom = document.querySelectorAll(".bathroom");
    handleClickBathRoom.forEach((bed) => {
      bed.addEventListener("click", () => {
        document
          .querySelector(".activebathroom")
          .classList.remove("activebathroom");
        bed.classList.add("activebathroom");
      });
    });

    const handleSelectHouse = document.querySelectorAll(".chose-house");

    handleSelectHouse.forEach((house) => {
      house.addEventListener("click", () => {
        document
          .querySelector(".active-select-house")
          .classList.remove("active-select-house");

        house.classList.add("active-select-house");
      });
    });
  });

  const onChangeAppliance = (checkedValues: CheckboxValueType[]) => {
    console.log("checked = ", checkedValues);
  };

  const [showAppliance, setShowAppliance] = useState(false);
  return (
    <div className="text-left ">
      <div className="mb-5">
        <p className="text-left text-[22px] font-semibold">Khoảng giá</p>
        <p className="text-left text-sm ">
          Tổng giá trung bình cho 5 đêm là $262
        </p>
        <div className="flex justify-center rotate-180 mt-5">
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-3 bg-black ml-2" />
          <div className="w-2 h-4 bg-black ml-2" />
          <div className="w-2 h-4 bg-black ml-2" />
          <div className="w-2 h-2 bg-black ml-2" />
          <div className="w-2 h-7 bg-black ml-2" />
          <div className="w-2 h-8 bg-black ml-2" />
          <div className="w-2 h-8 bg-black ml-2" />
          <div className="w-2 h-4 bg-black ml-2" />
          <div className="w-2 h-5 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-5 bg-black ml-2" />
          <div className="w-2 h-5 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-4 bg-black ml-2" />
          <div className="w-2 h-5 bg-black ml-2" />
          <div className="w-2 h-10 bg-black ml-2" />
          <div className="w-2 h-7 bg-black ml-2" />
          <div className="w-2 h-8 bg-black ml-2" />
          <div className="w-2 h-9 bg-black ml-2" />
          <div className="w-2 h-10 bg-black ml-2" />
          <div className="w-2 h-11 bg-black ml-2" />
          <div className="w-2 h-12 bg-black ml-2" />
          <div className="w-2 h-14 bg-black ml-2" />
          <div className="w-2 h-10 bg-black ml-2" />
          <div className="w-2 h-12 bg-black ml-2" />
          <div className="w-2 h-8 bg-black ml-2" />
          <div className="w-2 h-9 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-9 bg-black ml-2" />
          <div className="w-2 h-8 bg-black ml-2" />
          <div className="w-2 h-7 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-6 bg-black ml-2" />
          <div className="w-2 h-7 bg-black ml-2" />
          <div className="w-2 h-3 bg-black ml-2" />
        </div>
        <div>
          <Slider
            range={{ draggableTrack: true }}
            defaultValue={[0, 700]}
            max={700}
            min={10}
          />
        </div>
        <div className="flex justify-between">
          <div>
            <p className="text-left">Tối thiểu</p>
            <input
              type="text"
              placeholder="10"
              className="border-gray-400 border-2 w-52 rounded-md p-2"
            />
          </div>

          <div>
            <p className="text-left">Tối đa</p>
            <input
              type="text"
              placeholder="700+"
              className="border-gray-400 border-2 w-52 rounded-md p-2"
            />
          </div>
        </div>
      </div>
      <hr />
      <div className="mb-5">
        <p className="text-left text-[22px] font-semibold ">Loại nơi ở</p>
        <Checkbox.Group style={{ width: "100%" }} onChange={onChange}>
          <Row>
            <Col span={12} className="text-left">
              <Checkbox value="A">
                <p className="text-base font-medium">Toàn bộ nhà</p>
                <p className="text-sm">Toàn bộ nơi ở dành riêng cho bạn</p>
              </Checkbox>
            </Col>
            <Col span={12} className="text-left">
              <Checkbox value="B">
                <p className="text-base font-medium">Phòng</p>
                <p className="text-sm">
                  Bạn sẽ có phòng riêng và được sử dụng các khu vực chung
                </p>
              </Checkbox>
            </Col>
            <Col span={12} className="text-left">
              <Checkbox value="C">
                <p className="text-base font-medium">Phòng chung</p>
                <p className="text-sm">
                  Không gian để ngủ và các khu vực có thể sinh hoạt chung với
                  người khác
                </p>
              </Checkbox>
            </Col>
          </Row>
        </Checkbox.Group>
      </div>
      <hr />
      <div className="mb-5">
        <h2 className="text-xl font-semibold">Phòng và phòng ngủ</h2>
        <div>
          <p className="mt-2 mb-1">Phòng ngủ</p>
          <div className="bedroom-list flex text-center">
            <div className="bedRoom activeBedroom">Bất kỳ</div>
            <div className="bedRoom">1</div>
            <div className="bedRoom">2</div>
            <div className="bedRoom">3</div>
            <div className="bedRoom">4</div>
            <div className="bedRoom">5</div>
            <div className="bedRoom">6</div>
            <div className="bedRoom">7</div>
            <div className="bedRoom">8+</div>
          </div>
        </div>
        <div className="">
          <p className="mt-2 mb-1">Giường</p>
          <div className="bed-list flex text-center">
            <div className="bed activeBed">Bất kỳ</div>
            <div className="bed">1</div>
            <div className="bed">2</div>
            <div className="bed">3</div>
            <div className="bed">4</div>
            <div className="bed">5</div>
            <div className="bed">6</div>
            <div className="bed">7</div>
            <div className="bed">8+</div>
          </div>
        </div>
        <div className="">
          <p className="mt-2 mb-1">Phòng tắm</p>
          <div className="bathroom-list flex text-center">
            <div className="bathroom activebathroom">Bất kỳ</div>
            <div className="bathroom">1</div>
            <div className="bathroom">2</div>
            <div className="bathroom">3</div>
            <div className="bathroom">4</div>
            <div className="bathroom">5</div>
            <div className="bathroom">6</div>
            <div className="bathroom">7</div>
            <div className="bathroom">8+</div>
          </div>
        </div>
      </div>
      <hr />
      <div className="mb-5">
        <h2 className="text-xl font-semibold">Loại nhà/phòng</h2>
        <div className="mt-2 flex justify-between">
          <div className="chose-house active-select-house border-[1px] border-gray-400 w-[145px] h-[100px] rounded-2xl">
            <div className="p-3 ">
              <i className="fa fa-home fa-2xl mb-10"></i>
              <p className="font-semibold">Nhà</p>
            </div>
          </div>
          <div className="chose-house border-[1px] border-gray-400 w-[145px] h-[100px] rounded-2xl">
            <div className="p-3 ">
              <i className="fa-solid fa-tree-city fa-2xl mb-10"></i>
              <p className="font-semibold">Căn hộ</p>
            </div>
          </div>
          <div className="chose-house border-[1px] border-gray-400 w-[145px] h-[100px] rounded-2xl">
            <div className="p-3 ">
              <i className="fa-solid fa-house-signal fa-2xl mb-10"></i>
              <p className="font-semibold">Nhà khách</p>
            </div>
          </div>
          <div className="chose-house border-[1px] border-gray-400 w-[145px] h-[100px] rounded-2xl">
            <div className="p-3 ">
              <i className="fa-solid fa-hotel fa-2xl mb-10"></i>
              <p className="font-semibold">Khách sạn</p>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div className="mb-5">
        <h2 className="text-xl font-semibold">Tiện nghi</h2>
        <p className="text-base">Đồ dùng thiết yếu</p>
        <div>
          <Checkbox.Group
            style={{ width: "100%" }}
            onChange={onChangeAppliance}
          >
            <Row className="">
              <Col span={8}>
                <Checkbox value="A">Wifi</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value="B">Bếp</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value="C">Máy giặc</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value="D">Máy sấy quần áo</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value="E">Điều hòa nhiệt độ</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value="F">Hệ thống sưởi</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
          {showAppliance && (
            <div>
              <p className="text-base">Đặc điểm</p>
              <Checkbox.Group
                style={{ width: "100%" }}
                onChange={onChangeAppliance}
              >
                <Row className="">
                  <Col span={8}>
                    <Checkbox value="A">Bể bơi</Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="B">Bửa sáng</Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="C">Lò sưởi</Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="D">Giường king</Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="E">Cho phép hút thuốc</Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="F">Chỗ để xe miễn phí</Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
              <p className="text-base">Vị trí</p>
              <Checkbox.Group
                style={{ width: "100%" }}
                onChange={onChangeAppliance}
              >
                <Row className="">
                  <Col span={8}>
                    <Checkbox value="A">Hướng biển </Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="B">Ven sông/hồ/biển</Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
              <p className="text-base">An toàn</p>
              <Checkbox.Group
                style={{ width: "100%" }}
                onChange={onChangeAppliance}
              >
                <Row className="">
                  <Col span={8}>
                    <Checkbox value="A">Máy báo khói </Checkbox>
                  </Col>
                  <Col span={8}>
                    <Checkbox value="B">Máy phát hiện khí CO</Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
            </div>
          )}

          <button className="font-semibold mt-5 underline" onClick={() => setShowAppliance(!showAppliance)}>
            {showAppliance ? 'Ẩn': 'Hiển thị thêm'}
          </button>
        </div>
      </div>
    </div>
  );
};

export default FilterModal;
