export const LocationModalDetail = () => {
  return (
    <div className="grid grid-cols-12 border-2 rounded-full m-3 w-[850px] ml-20 ">
      <div className="col-span-4 border-r-2 hover:bg-gray-300 hover:rounded-full py-3 pl-5 hover:transition hover:ease-in-out hover:delay-75">
        <h2 className="text-sm text-black font-semibold">Địa điểm</h2>
        <input type="text" placeholder="Tìm kiếm điểm đến" className="outline-none border-none rounded-md opacity-50  !text-black" />
      </div>
      <div className="col-span-2 border-r-2 text-center py-3 px-2  hover:bg-gray-300 hover:rounded-full hover:transition hover:ease-in-out hover:delay-75">
        <h2 className="text-sm text-black font-semibold">Nhận phòng</h2>
        <p className="text-sm text-gray-500">Thêm ngày</p>
      </div>
      <div className="col-span-2 border-r-2  text-center py-3 px-2  hover:bg-gray-300 hover:rounded-full hover:transition hover:ease-in-out hover:delay-75">
        <h2 className="text-sm text-black font-semibold">Trả phòng</h2>
        <p className="text-sm text-gray-500">Thêm ngày</p>
      </div>
      <div className="col-span-4 pl-5 flex justify-between py-3  hover:bg-gray-300 hover:rounded-full pr-4 hover:transition hover:ease-in-out hover:delay-75">
        <div>
          <h2 className="text-sm text-black font-semibold">Khách</h2>
          <p className="text-sm text-gray-500">Thêm khách</p>
        </div>
        <div className="bg-[#ff385c] rounded-3xl p-3 text-white text-[15px] px-4">
          <i className="fa fa-search"></i>
        </div>
      </div>
    </div>
  );
};

export default LocationModalDetail;
