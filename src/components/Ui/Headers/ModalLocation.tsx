import { DingdingOutlined, UserOutlined } from "@ant-design/icons";

import { ExperienceModalDetail, ExprienceOnlModalDetail, LocationModalDetail } from ".";
import { TabsProps } from "antd";
import { Avatar, Popover, Tabs } from "..";

export const ModalLocation = ({ isVisible, onClose }) => {
  if (!isVisible) return null;

  const onChange = (key: string) => {
    console.log(key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Chỗ ở",
      children: <LocationModalDetail />,
    },
    {
      key: "2",
      label: "Trải Nghiệm",
      children: <ExperienceModalDetail />,
    },
    {
      key: "3",
      label: "Trải nghiệm trực tuyến",
      children: <ExprienceOnlModalDetail />,
    },
  ];

  return (
    <div
      className="fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm z-20 "
      onClick={() => onClose()}
    >
      <div className="w-full ">
        <div className="grid grid-cols-12 bg-white pt-6 ">
          <div className=" logo col-span-2 pb-40 pl-14">
            <div className="text-2xl font-bold text-[#ff385c] flex cursor-pointer max-w-[1400px] mx-auto">
              <div className="">
                <DingdingOutlined />
              </div>
              <div className="mt-1 ml-1">Airbnb</div>
            </div>
          </div>
          <div className="flex col-span-7 ">
            <Tabs
              centered
              defaultActiveKey="1"
              items={items}
              onChange={onChange}
            />
          </div>
          <div className="flex items-center justify-end col-span-3 fixed top-6 right-20">
            <p className="text-[14px] text-[#22222] font-bold ">
              Cho thuê chổ ở qua Airbnb
            </p>
            <div className="mx-4">
              <i className="fa fa-globe"></i>
            </div>

            <Popover
              content={
                <div className="">
                  <h2 className="font-bold !my-4 p-2">Nguyễn Văn A</h2>
                  <hr />
                  <div className=" p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                    Cho thuê chỗ ở qua Airbnb
                  </div>
                  <div className="p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                    Trung tâm trợ giúp
                  </div>
                  <div className="p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                    Đăng xuất
                  </div>
                </div>
              }
              trigger="click"
            >
              <div className="border-2 px-2 py-1 rounded-2xl hover:shadow-md">
                <i className="fa fa-bars"></i>
                <Avatar
                  size={30}
                  icon={<UserOutlined />}
                  className="!ml-2 !bg-[#717171]"
                />
              </div>
            </Popover>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalLocation;
