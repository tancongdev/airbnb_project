import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../style.css";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant/configPath";
import { useState } from "react";

function SampleNextArrow(props: { className: any; style: any; onClick: any }) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", color: "black" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props: { className: any; style: any; onClick: any }) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", color: "black" }}
      onClick={onClick}
    />
  );
}

export const HeaderMultipleSlick = () => {
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 7,
    speed: 500,
    rows: 1,
    slidesPerRow: 1,
    nextArrow: (
      <SampleNextArrow
        className={undefined}
        style={undefined}
        onClick={undefined}
      />
    ),
    prevArrow: (
      <SamplePrevArrow
        className={undefined}
        style={undefined}
        onClick={undefined}
      />
    ),
  };

  const navigate = useNavigate();

  const [active1, setActive1] = useState(false);
  const [active2, setActive2] = useState(false);
  const [active3, setActive3] = useState(false);
  const [active4, setActive4] = useState(false);
  const [active5, setActive5] = useState(false);
  const [active6, setActive6] = useState(false);
  const [active7, setActive7] = useState(false);
  const [active8, setActive8] = useState(false);
  const [active9, setActive9] = useState(false);

  const handleClick1 = () => {
    setActive1(true);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick2 = () => {
    setActive1(false);
    setActive2(true);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick3 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(true);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick4 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(true);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick5 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(true);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick6 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(true);
    setActive7(false);
    setActive8(false);
    setActive9(false);
  };

  const handleClick7 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(true);
    setActive8(false);
    setActive9(false);
  };

  const handleClick8 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(true);
    setActive9(false);
  };

  const handleClick9 = () => {
    setActive1(false);
    setActive2(false);
    setActive3(false);
    setActive4(false);
    setActive5(false);
    setActive6(false);
    setActive7(false);
    setActive8(false);
    setActive9(true);
  };

  let classRoom = 'border-b-2 border-black text-center'
  
  return (
    <Slider {...settings} className='text-center z-10'>
      <div
        className={`btnsRoom text-center ${active1 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.khungCanhTuyetVoi);
          handleClick1();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/khungcanh.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Khung cảnh tuyệt vời</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active2 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.nhaNho);
          handleClick2();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/nhanho.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Nhà nhỏ</h3>
      </div>
      <div
       className={`btnsRoom text-center ${active3 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.luotSong);
          handleClick3();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/luotsong.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Lướt sóng</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active4 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.hoBoi);
          handleClick4();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/hoboi.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Hồ bơi tuyệt vời</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active5 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.leuMucDong);
          handleClick5();
        }}
      >
        <div className="flex justify-center ">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/leumucdong.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Lều mục đồng</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active6 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.ryokan);
          handleClick6();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/ryokan.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Ryokan</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active7 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.nhaTrenCay);
          handleClick7();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/nhatrencay.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Nhà trên cây</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active8 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.thanhPhoNoiTieng);
          handleClick8();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/thanhphonoitieng.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Thành phố nổi tiếng</h3>
      </div>
      <div
        className={`btnsRoom text-center ${active9 ? classRoom : ''}`}
        onClick={() => {
          navigate(PATH.hanok);
          handleClick9();
        }}
      >
        <div className="flex justify-center">
          <img
            style={{ width: "24px" }}
            src="../../../public/img/iconHeader/hanok.jpg"
            alt=""
          />
        </div>
        <h3 className="text-xs w-border">Hanok</h3>
      </div>
    </Slider>
  );
};

export default HeaderMultipleSlick;
