export const NgoaiTroi = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Lake Martin</p>
        <p>Chỗ ở cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Banff</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nerja</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">greer</p>
        <p>Căn hộ cho thuê </p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Lake Havasu City</p>
        <p>Nhà cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Lake Powell</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">North Rim</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Payson</p>
        <p>Căn hộ cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Pinetop-Lakeside</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Red-Rock</p>
        <p>Cabin cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Dinner Plain</p>
        <p>Chỗ ở cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Streaky Bay</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Emarald Lake </p>
        <p>Cabin cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Vancouver Island</p>
        <p>Căn hộ cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Victorya</p>
        <p>Chỗ ở cho thuê hướng biển</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Idyllwild-Pine Cove</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Mamoth Lakes</p>
        <p>Nhà cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Bantaci</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
    </div>
  );
};

export default NgoaiTroi;
