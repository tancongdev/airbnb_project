export const DanhMuc = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Hồ bơi tuyệt vời</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Bắc cực</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Khu cắm trại</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Xe cấm trại</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Lâu đài</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Container</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nông thôn</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Thiết kế</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nhà dưới lòng đất</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nông trại</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Công viên quốc gia</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Vườn nho</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Thật ấn tượng!</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nhà nhỏ</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Tháp</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Cối xây gió</p>
      </div>
    </div>
  );
};

export default DanhMuc;
