export const BaiBien = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Rhodes</p>
        <p>Chỗ ở cho thuê có hồ bơi</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Nashville</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Dublin</p>
        <p>Chỗ ở cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Canmore</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Pinetop-Lakeside</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Red-Rock</p>
        <p>Cabin cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Dinner Plain</p>
        <p>Chỗ ở cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Streaky Bay</p>
        <p>Nhà nghĩ dưỡng cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Emarald Lake </p>
        <p>Cabin cho thuê</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Vancouver Island</p>
        <p>Căn hộ cho thuê</p>
      </div>
    </div>
  );
};

export default BaiBien;
