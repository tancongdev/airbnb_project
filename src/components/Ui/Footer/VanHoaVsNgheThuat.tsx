

export const VanHoaVsNgheThuat = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Phoenix</p>
            <p>Căn hộ cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Hot Springs</p>
            <p>Nhà cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Los Angeles</p>
            <p>Căn hộ cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">San Diego</p>
            <p>Căn hộ cho thuê hướng biển</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">San Francisco</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Barcelona</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Prague</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Washington</p>
            <p>Căn hộ cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Keswick</p>
            <p>Cabin cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">London</p>
            <p>Nhà cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Scarborough</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Sherwood Forest</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">York</p>
            <p>Chỗ ở cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Paris</p>
            <p>Căn hộ cao cấp cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Rhodes</p>
            <p>Chỗ ở cho thuê có hồ bơi</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Nashville</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Dublin</p>
            <p>Chỗ ở cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Canmore</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>

    </div>
  )
}

export default VanHoaVsNgheThuat
