export const TraiNghiem = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">London</p>
        <p>England</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Paris</p>
        <p>Île-de-France</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">New York</p>
        <p>New York</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Barcelona</p>
        <p>Catalonia</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Istanbul</p>
        <p>Istanbul</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Bali</p>
        <p>Indonesia</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Amsterdam</p>
        <p>North Holland</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Miami</p>
        <p>Florida</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Madrid</p>
        <p>

Community of Madrid</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Los Angeles</p>
        <p>Califonia</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Rome</p>
        <p>Lazio</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Lizbon</p>
        <p>Lizbon</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Tokyo</p>
        <p>Tokyo</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Vienna</p>
        <p>Vienna</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Athens</p>
        <p>Hy Lạp</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Prague</p>
        <p>Séc</p>
      </div>
      <div className="col-span-2 cursor-pointer">
        <p className=" font-semibold">Orlando</p>
        <p>Florida</p>
      </div>
    </div>
  );
};

export default TraiNghiem;
