

export const PhobienF = () => {
  return (
    <div className="grid grid-cols-12 gap-4">
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Canmore</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Benalmádena</p>
            <p>Nhà cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Marbella</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Mijas</p>
            <p>Căn hộ cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Prescott</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Scottsdale</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Tucson</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Jasper</p>
            <p>Cabin cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Mountain View</p>
            <p>Cabin cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Devonport</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Mallacoota</p>
            <p>Chỗ ở cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Ibiza</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Anaheim</p>
            <p>căn cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Monterey</p>
            <p>Nhà nghĩ thôn dã cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Paso Robles</p>
            <p>Nhà nghĩ thôn dã cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Santa Barbara</p>
            <p>Chỗ ở cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Sonoma</p>
            <p>Chỗ ở cho thuê</p>
        </div>
        <div className="col-span-2 cursor-pointer">
            <p className=" font-semibold">Canmore</p>
            <p>Nhà nghĩ dưỡng cho thuê</p>
        </div>

    </div>
  )
}

export default PhobienF

