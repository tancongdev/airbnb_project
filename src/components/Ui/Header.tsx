import { DingdingOutlined, UserOutlined } from "@ant-design/icons";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { Avatar, Popover, Switch } from ".";
import { HeaderMultipleSlick, ModalLocation } from "./Headers";
import { Modal } from "antd";
import FilterModal from "./Headers/FilterModal";
import { PATH } from "constant/configPath";
import { useAuth } from "hooks/useAuth";

export const Header = () => {
  const onChange = (checked: boolean) => {
    console.log(`switch to ${checked}`);
  };

  const navigate = useNavigate();

  const [showModal, setShowModal] = useState(false);

  const [isModalFilter, setIsModalFilter] = useState(false);

  const showModalfilter = () => {
    setIsModalFilter(true);
  };

  const handleOk = () => {
    setIsModalFilter(false);
  };

  const handleCancel = () => {
    setIsModalFilter(false);
  };

  const { user } = useAuth();
  return (
    <header>
      <div className=" border-b-2 my-2 ">
        <div className="grid grid-cols-4 max-w-[1400px] mx-auto ">
          <div className="my-4 logo col-span-1 cursor-pointer">
            <div
              className="text-2xl font-bold text-[#ff385c] flex cursor-pointer"
              onClick={() => {
                navigate("/");
                location.reload();
              }}
            >
              <div className="">
                <DingdingOutlined />
              </div>
              <div className="mt-1 ml-1">Airbnb</div>
            </div>
          </div>

          <div className="col-span-2 ml-8 py-1 cursor-pointer">
            <div className="flex items-center border-2 p-1 rounded-3xl m-3 w-[390px] shadow-md ml-20 py-1">
              <div
                className="border-r-2 px-2 font-bold text-sm"
                onClick={() => {
                  setShowModal(true);
                }}
              >
                Địa điểm bất kỳ
              </div>
              <div
                className="border-r-2 px-2 font-bold text-sm"
                onClick={() => {
                  setShowModal(true);
                }}
              >
                tuần bất kỳ
              </div>
              <div>
                <div
                  className="flex ml-2"
                  onClick={() => {
                    setShowModal(true);
                  }}
                >
                  <input
                    style={{ outline: "none", width: "120px" }}
                    type="text"
                    placeholder="Thêm khách"
                  />
                  <div className="bg-[#ff385c] rounded-3xl p-2 px-3 text-white text-[10px]">
                    <i className="fa fa-search"></i>
                  </div>
                </div>
              </div>
            </div>
            <ModalLocation
              isVisible={showModal}
              onClose={() => setShowModal(false)}
            />
          </div>

          <div className="flex items-center justify-end col-span-1 cursor-pointer">
            <p className="text-[14px] text-[#22222] font-bold ">
              Cho thuê chổ ở qua Airbnb
            </p>
            <div className="mx-4">
              <i className="fa fa-globe"></i>
            </div>
            {user && (
              <Popover
                content={
                  <div className="">
                    <h2 className="font-bold !my-4 p-2">Nguyễn Văn A</h2>
                    <hr />
                    <div className=" p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                      Cho thuê chỗ ở qua Airbnb
                    </div>
                    <div className="p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                      Trung tâm trợ giúp
                    </div>
                    <div className="p-2 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                      Đăng xuất
                    </div>
                  </div>
                }
                trigger="click"
              >
                <div className="border-2 px-2 py-1 rounded-2xl hover:shadow-md">
                  <i className="fa fa-bars"></i>
                  <Avatar
                    size={30}
                    icon={<UserOutlined />}
                    className="!ml-2 !bg-[#717171]"
                  />
                </div>
              </Popover>
            )}
            {!user && (
              <p
                onClick={() => {
                  navigate(PATH.login);
                }}
              >
                Đăng nhập
              </p>
            )}
          </div>
        </div>
      </div>
      <div className="max-w-[1400px] mx-auto ">
        <div className="grid grid-cols-12 my-4">
          <div className="col-span-9 px-6">
            <HeaderMultipleSlick />
          </div>
          <div className="col-span-1 ml-2 ">
            <div
              className="flex justify-center items-center border-2 rounded-lg py-1 cursor-pointer"
              onClick={showModalfilter}
            >
              <i className="fa fa-filter"></i>
              <p className="pl-1">Bộ lọc</p>
            </div>

            <Modal
              title="Bộ lọc"
              className="text-center !w-[700px] overflow-auto max-h-[600px]"
              open={isModalFilter}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <FilterModal />
            </Modal>
          </div>
          <div className="col-span-2 ml-2">
            <div className="flex justify-center items-center border-2 rounded-lg py-1">
              <p className="text-sm mr-2">Hiển thị tổng trước thuế</p>
              <Switch defaultChecked onChange={onChange} />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
