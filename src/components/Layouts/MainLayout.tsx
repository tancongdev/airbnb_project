import { FooterMain, Header } from "components/Ui";
import { Outlet } from "react-router-dom";

export const MainLayout = () => {
  return (
    <div>
      <div className="border-b-2 fixed w-full z-20 bg-white ">
        <Header />
      </div>
      <div className="pt-20">
        <Outlet />
      </div>
      <div>
        <FooterMain />
      </div>
    </div>
  );
};

export default MainLayout;
