import { FooterMain, Header } from "components/Ui";
import { Outlet } from "react-router-dom";

const AutLayout = () => {
  return (
    <div>
      <div className="border-b-2 w-full z-20 bg-white ">
        <Header />
      </div>
      <div className="my-20 ">
        <Outlet />
      </div>
      <div>
        <FooterMain />
      </div>
    </div>
  );
};

export default AutLayout;
