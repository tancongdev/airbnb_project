declare type ApiRespone<T> = {
    statusCode: number,
    message: string,
    content: T
}