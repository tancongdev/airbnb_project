import {combineReducers } from '@reduxjs/toolkit'
import { quanLyPhongReducer } from './QuanLyPhim/slice'
import { quanLyNguoiDungReducer } from './QuanLyNguoiDung/slice'

export const rootReducer = combineReducers({
    quanLyPhongReducer: quanLyPhongReducer,
    quanLyNguoiDungReducer: quanLyNguoiDungReducer
})