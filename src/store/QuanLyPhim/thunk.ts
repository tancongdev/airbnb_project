import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhongServices } from "services/quanLyPhongServices";



export const getListRoomThunk = createAsyncThunk("quanLyPhong/getListRoomThunk", async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyPhongServices.getListRoom()
        return data.data.content
    } catch (error) {
        rejectWithValue(error)
        
    }
})