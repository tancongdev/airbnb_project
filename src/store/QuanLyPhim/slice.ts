import { createSlice } from '@reduxjs/toolkit'
import { getListRoomThunk } from './thunk';
import { Room } from 'types/quanLyPhong';

type QuanLyPhongInitialState = {
    listRoom: Room[],
    isFetchListRoom: boolean

}

const initialState: QuanLyPhongInitialState = {
    listRoom: [],
    isFetchListRoom: false
}

const quanLyPhongSlice = createSlice({
  name: "quanLyPhong",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
      builder
      .addCase(getListRoomThunk.fulfilled, (state, {payload})=> {
        state.listRoom = payload
        state.isFetchListRoom = false
      })
      .addCase(getListRoomThunk.rejected, (state)=> {
        state.isFetchListRoom = false
      })
      .addCase(getListRoomThunk.pending, (state)=> {
        state.isFetchListRoom = true
      })
  },
});

export const {actions: quanLyPhongActions, reducer: quanLyPhongReducer} = quanLyPhongSlice

