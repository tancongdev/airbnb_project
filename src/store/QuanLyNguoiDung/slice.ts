import { createSlice } from '@reduxjs/toolkit'
import { User } from 'types/quanLyNguoiDung'
import { loginThunk } from './thunk'


type QuanLyNguoiDungInittialState = {
  user?:  User
  accessToken?: any
}
const initialState: QuanLyNguoiDungInittialState = {
  accessToken: localStorage.getItem("accessToken")
}

const quanLyNguoiDungSlice = createSlice({
  name: 'quanLyNguoiDungSlice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loginThunk.fulfilled, (state, {payload})=> {
      console.log('payload: ', payload);
      state.user = payload
      if(payload) {
        
        localStorage.setItem('accessToken', (payload.accessToken))
       
      }
    })

  }
});

export const {actions:quanLyNguoiDungActions, reducer: quanLyNguoiDungReducer } = quanLyNguoiDungSlice

