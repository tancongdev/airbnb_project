import { createAsyncThunk } from "@reduxjs/toolkit"
import { loginSchemaTypes } from "schema/LoginSchema"
import { quanLyNguoiDungServices } from "services/quanLyNguoiDung"





export const loginThunk = createAsyncThunk('quanLyNguoiDung/loginThunk', async (payload: loginSchemaTypes, {rejectWithValue}) => {
    
    try {
        const data = await quanLyNguoiDungServices.login(payload)
        console.log('data: ', data.data.content);
        return data.data.content
    } catch (err) {
        return rejectWithValue(err)
    }
    
})