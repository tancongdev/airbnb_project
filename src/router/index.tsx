import { Hanok, HoBoi, KhungCanhTuyetVoi, LeuMucDong, NhaNho, NhaTrenCay, Ryokan, ThanhPhoNoiTieng } from "components";
import { MainLayout } from "components/Layouts";
import AutLayout from "components/Layouts/AutLayout";
import { HomeTamplate } from "components/Template";
import LoginTemplate from "components/Template/LoginTemplate";
import RegisterTemplate from "components/Template/RegisterTemplate";
import { PATH } from "constant/configPath";
import { RouteObject } from "react-router-dom";


export const router: RouteObject[] = [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <HomeTamplate/>
      },
      {
        path: PATH.khungCanhTuyetVoi,
        element: <KhungCanhTuyetVoi/>
      },
      {
        path: PATH.thanhPhoNoiTieng,
        element: <ThanhPhoNoiTieng/>
      },
      {
        path: PATH.hoBoi,
        element: <HoBoi/>
      },
      {
        path: PATH.luotSong,
        element: <NhaTrenCay/>
      },
      {
        path: PATH.hanok,
        element: <Hanok/>
      },
      {
        path: PATH.leuMucDong,
        element: <LeuMucDong/>
      },
      {
        path: PATH.ryokan,
        element: <Ryokan/>
      },
      {
        path: PATH.nhaNho,
        element: <NhaNho/>
      },
      {
        path: PATH.nhaTrenCay,
        element: <NhaTrenCay/>
      }

    ],
  },
  {
    element: <AutLayout/>,
    children: [
      {
        path: PATH.register,
        element: <RegisterTemplate/>
      },
      {
        path: PATH.login,
        element: <LoginTemplate/>
      }
    ]
  }
];
